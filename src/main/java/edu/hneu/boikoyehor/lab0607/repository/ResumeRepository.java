package edu.hneu.boikoyehor.lab0607.repository;

import edu.hneu.boikoyehor.lab0607.model.Resume;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResumeRepository extends JpaRepository<Resume, Long> {
}
