package edu.hneu.boikoyehor.lab0607.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ResumeDTO {
    private Long id;
    private String lastName;
    private String firstName;
    private String middleName;
    private String phone;
    private String email;
    private String specialization;
    private Integer age;
    private String gender;
    private String education;
    private String workExperience;
    private Double desiredSalary;
    private String other;
    private LocalDateTime dateAdded;
    private LocalDateTime dateModified;
}
