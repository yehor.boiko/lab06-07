package edu.hneu.boikoyehor.lab0607.controller;

import edu.hneu.boikoyehor.lab0607.dto.ResumeDTO;
import edu.hneu.boikoyehor.lab0607.model.Resume;
import edu.hneu.boikoyehor.lab0607.service.ResumeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
public class ResumeController {
    private static final ModelMapper mapper = new ModelMapper();

    @Autowired
    private ResumeService resumeService;
    private static final String EMAIL_REGEX = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
            + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";

    @GetMapping("")
    public String showHomePage() {
        return "index";
    }

    @GetMapping("/resumes")
    public String showAllResumes(Model model) {
        model.addAttribute("resumes", resumeService.findAll());
        System.out.println(resumeService.findAll());
        return "showAll";
    }

    @GetMapping("/resumes/insert")
    public String showInsertForm(Model model) {
        model.addAttribute("resume", new ResumeDTO());
        return "edit_insert";
    }

    @PostMapping("/resumes")
    public String createResume(@ModelAttribute("resume") ResumeDTO resumeDTO, Model model) {
        Optional<String> error = validateForm(resumeDTO);
        if(error.isPresent()) {
            model.addAttribute("error", error.get());
            return "edit_insert";
        }

        Resume resume = mapper.map(resumeDTO, Resume.class);
        resumeService.save(resume);
        return "redirect:/resumes";
    }

    @GetMapping("/resumes/edit/{id}")
    public String showEditForm(@PathVariable("id") String idString, Model model) {
        Long id = Long.parseLong(idString);
        Resume resume = resumeService.findById(id);
        model.addAttribute("resume", mapper.map(resume, ResumeDTO.class));
        return "edit_insert";
    }

    @PostMapping("/resumes/{id}")
    public String updateResume(@PathVariable("id") Long id, @ModelAttribute("resume") ResumeDTO resumeDTO, Model model) {
        Optional<String> error = validateForm(resumeDTO);
        System.out.println(error);
        if(error.isPresent()) {
            model.addAttribute("error", error.get());
            return "edit_insert";
        }

        Resume resumeDateAdded = resumeService.findById(id);
        Resume resume = mapper.map(resumeDTO, Resume.class);
        resume.setId(id);
        // TODO.
        resume.setDateAdded(resumeDateAdded.getDateAdded());
        resumeService.save(resume);
        return "redirect:/resumes";
    }

    @GetMapping("/resumes/delete/{id}")
    public String deleteResume(@PathVariable("id") Long id) {
        resumeService.deleteById(id);
        return "redirect:/resumes";
    }

    private Optional<String> validateForm(ResumeDTO resumeDTO) {
        // Check if required fields are empty.
        if (resumeDTO.getLastName() == null || resumeDTO.getLastName().isEmpty()
                || resumeDTO.getFirstName() == null || resumeDTO.getFirstName().isEmpty()
                || resumeDTO.getPhone() == null || resumeDTO.getPhone().isEmpty()
                || resumeDTO.getEmail() == null || resumeDTO.getEmail().isEmpty()
                || resumeDTO.getSpecialization() == null || resumeDTO.getSpecialization().isEmpty()
                || resumeDTO.getWorkExperience() == null || resumeDTO.getWorkExperience().isEmpty()) {
            return Optional.of("Будь ласка, заповніть всі необхідні поля");
        }

        // Check field length.
        if (resumeDTO.getLastName().length() > 32 || resumeDTO.getFirstName().length() > 16
                || resumeDTO.getMiddleName().length() > 32 || resumeDTO.getPhone().length() > 16
                || resumeDTO.getEmail().length() > 255 || resumeDTO.getSpecialization().length() > 64
                || resumeDTO.getEducation().length() > 255 || resumeDTO.getOther().length() > 255) {
            return Optional.of("Деякі з полей перевищують максимально можливу довжину");
        }

        // Validate email.
        /* Pattern pattern = Pattern.compile(EMAIL_REGEX);
        Matcher matcher = pattern.matcher(resumeDTO.getEmail());
        if(!matcher.matches()) {
            return Optional.of("Не валідний email");
        };
        if (resumeService.isEmailExists(resumeDTO.getEmail(), id)) {
            request.setAttribute("errorMessage", "Даний email вже зареєстрований");
            if (id != null) {
                Resume resume = resumeService.findById(id);
                request.setAttribute("Resume", resume);
            }
            request.getRequestDispatcher(INSERT_OR_EDIT_RESUME).forward(request, response);
            return;
        } */


        // Validate integer value after parsing.
        try {
            resumeDTO.setAge(Integer.parseInt(String.valueOf(resumeDTO.getAge())));
        } catch (NumberFormatException e) {
            return Optional.of("Неправильне значення віку (не ціле число)");
        }
        if(resumeDTO.getAge() < 10) {
            return Optional.of("Вибачте, Ви ще занадто молодий");
        }

        // Validate double value after parsing.
        try {
            resumeDTO.setDesiredSalary(Double.parseDouble(String.valueOf(resumeDTO.getDesiredSalary())));
        } catch (NumberFormatException e) {
            return Optional.of("Невірне значення для бажаної зарплати (не число)");
        }

        return Optional.empty();
    }

}
