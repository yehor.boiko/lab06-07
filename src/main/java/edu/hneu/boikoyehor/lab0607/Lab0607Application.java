package edu.hneu.boikoyehor.lab0607;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab0607Application {

	public static void main(String[] args) {
		SpringApplication.run(Lab0607Application.class, args);
	}

}
