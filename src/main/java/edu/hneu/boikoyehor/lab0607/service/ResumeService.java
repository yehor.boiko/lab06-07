package edu.hneu.boikoyehor.lab0607.service;

import edu.hneu.boikoyehor.lab0607.model.Resume;
import edu.hneu.boikoyehor.lab0607.repository.ResumeRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface ResumeService {
    public void save(Resume resume);
    public Resume findById(Long id);
    public void deleteById(Long id);
    public List<Resume> findAll();
}
