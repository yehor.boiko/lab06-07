package edu.hneu.boikoyehor.lab0607.service;

import edu.hneu.boikoyehor.lab0607.model.Resume;
import edu.hneu.boikoyehor.lab0607.repository.ResumeRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class ResumeServiceImpl implements ResumeService {
    @Autowired
    private ResumeRepository resumeRepository;

    public void save(Resume resume) {
        resumeRepository.save(resume);
    }

    public Resume findById(Long id) {
        return resumeRepository.findById(id).orElse(null);
    }

    public void deleteById(Long id) {
        resumeRepository.deleteById(id);
    }

    public List<Resume> findAll() {
        return resumeRepository.findAll();
    }
}
